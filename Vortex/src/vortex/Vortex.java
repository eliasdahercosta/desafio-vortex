package vortex;
import java.util.Scanner;
public class Vortex {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int T;
        int Q;
        int X;
        do{
            System.out.println("Digite quantos segundos faltam:");
            T = in.nextInt();
            if(T<1){
                System.out.println("Tempo menor que 1, como que joga sem tempo ou com tempo negativo?");
            }else if(T>25){
                System.out.println("Tempo maior que 25, lembra que tem até 24 segundos para fazer a jogada.");
            }
        }while (T<1 || T>25);
        do{
            System.out.println("Digite o quantos jogadores estão na quadra:");
            Q = in.nextInt();
            if(Q<2){
                System.out.println("Numero de jogadores menor que 2, é time de 1 ou de fantasma?");
            }else if(Q>5){
                System.out.println("Numero de jogadores maior que 5, time de até 5 pessoas.");
            }
        }while (Q<2 || Q>5);
        do{
            System.out.println("Digite o tempo de reposicionamento:");
            X = in.nextInt();
            if(X<1){
                System.out.println("Tempo menor que 1, o reposicionamento é instantâneo??");
            }else if(X>Q){
                System.out.println("Tempo maior que o numero de jogadores, aí não faz muito sentido.");
            }
        }while (X<1 || X>Q);
        int saida = 0;
        //iterações recorda quanto tempo passou.
        int iterações = 0;
        // Loop de for calcula a saída de Q-1 a Q-X
        for (iterações=0;iterações<X;iterações++){
            saida=saida+(Q-1-iterações);
        }
        // Q-X é o numero de jogadores livres que vai se repetir.
        // T-iterações é quantas vezes Q-X vai se repetir, pois é o numero de segundos que falta para o fim da jogada.
        saida = saida + ((Q-X)*(T-iterações));
        System.out.println("Resposta:"+saida);
    }
}