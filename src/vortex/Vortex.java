package vortex;
import java.util.Scanner;
public class Vortex {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Digite o valor de T");
        int T = in.nextInt();
        System.out.println("Digite o valor de Q");
        int Q = in.nextInt();
        System.out.println("Digite o valor de X");
        int X = in.nextInt();
        //Assume-se os valores de entrada T,Q,X = 7 5 3
        //No instante 0 observa-se um caso como este:
        
        //Tempo  Jogadores livres  Estado atual dos jogadores
        //0      4                 A/0/0/0/0
        
        //Onde A é o jogador que está com a bola e 0 é tempo de reposicionamento de cada jogador
        //Os jogadores livres são aqueles que não tem a bola e não tem tempo de reposicionamento restante.
        //No instante 0, o jogador 1 passa a bola para o jogador 2 e começa o seu tempo de espera
        //Portanto, o instante 1 fica assim:
        
        //Tempo  Jogadores livres  Estado atual dos jogadores
        //1      3                 2/A/0/0/0
        
        //Como já passou um segundo, o jogador 1 tem 2 segundos de espera sobrando.
        //Nessa logica, pode-se desenvolver os estados de tal forma:
        
        //Tempo  Jogadores livres  Estado atual dos jogadores
        //2      2                 1/2/A/0/0
        //3      2                 0/1/2/A/0
        //4      2                 0/0/1/2/A
        //5      2                 A/0/0/1/2
        //6      2                 2/A/0/0/1
        
        //O instante 7 é omitido por ser quando o tempo acaba, então logicamente não haverá passes.
        //Somando os jogadores livres, temos: (4+3+2+2+2+2+2) = (7+4+4+2) = 17
        //Batendo com a resposta do exemplo.
        //Confimando com outro exemplo, 3/3/2:
        //Tempo  Jogadores livres  Estado atual dos jogadores
        //0      2                 A/0/0
        //1      1                 1/A/0
        //2      1                 0/1/A
        //2+1+1 = 4
        int saida = 0;
        //Olhando os exemplos, pode-se observar 3 regras:
        //  1) Em tempo 0, os jogadores livres serão Q-1.
        //  2) A cada segundo que passa, os jogadores livres diminuem em 1, portanto é Q-1-(tempo que passou).
        //  3) Essas duas regras se aplicam até Q-X, onde os jogadores param de decreser.
        //Daí surga a logica a seguir:
        //iterações recorda quanto tempo passou para calulo de Q-T-1.
        int iterações = 0;
        // Loop de for calcula a saída de Q-1 a Q-X
        for (iterações=0;iterações<X;iterações++){
            saida=saida+(Q-1-iterações);
        }
        // Q-X é o numero de jogadores livres que vai se repetir.
        // T-iterações é quantas vezes Q-X vai se repetir, pois é o numero de segundos que falta.
        saida = saida + ((Q-X)*(T-iterações));
        System.out.println("Saida:"+saida);
    }
}
